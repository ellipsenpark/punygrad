# punygrad

Yet another Rust port of Andrej Karpathys' micrograd, with some inspiration from micrograd.rs

* https://github.com/karpathy/micrograd
* https://github.com/danielway/micrograd-rs

Created for personal learning purposes and fun. No claim to correctness or completeness.
