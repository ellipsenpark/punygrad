use std::cell::RefCell;
use std::collections::HashSet;
use std::hash::{Hash, Hasher};
use std::ops::AddAssign;
use std::rc::Rc;
use std::{ops, ptr};

// inner value structure
pub struct WrappedValue {
    pub data: f32,
    pub grad: f32,

    pub prev: Vec<Value>,

    // for debugging, graphviz, etc ...
    pub op: String,
}

impl WrappedValue {
    pub fn new(val: f32) -> WrappedValue {
        WrappedValue {
            data: val,
            grad: 0.0,
            prev: vec![],
            op: "".to_string(),
        }
    }
}

// outer value structure to achieve some kind
// of random mutability ...
#[derive(Clone)]
pub struct Value {
    pub val: Rc<RefCell<WrappedValue>>,
    pub bw: Rc<RefCell<dyn Fn()>>,
}

// we need to be able to compare values by their
// reference (whether they point to the same inner value)
impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.val, &other.val)
    }
}

impl Eq for Value {}

// for topological sorting, we want to sort values
// in a hash set ...
impl Hash for Value {
    fn hash<H: Hasher>(&self, state: &mut H) {
        ptr::hash(self.val.as_ptr(), state);
    }
}

impl Value {
    pub fn new(val: f32) -> Self {
        Value {
            val: Rc::new(RefCell::new(WrappedValue::new(val))),
            bw: Rc::new(RefCell::new(|| {})),
        }
    }

    pub fn copy(&self) -> Value {
        Value {
            val: Rc::clone(&self.val),
            bw: Rc::clone(&self.bw),
        }
    }

    pub fn grad(&self) -> f32 {
        self.val.borrow().grad
    }

    pub fn data(&self) -> f32 {
        self.val.borrow().data
    }

    pub fn powf(&self, rhs: f32) -> Value {
        let wrapped_out = WrappedValue {
            data: self.val.borrow().data.powf(rhs),
            grad: 0.0,
            prev: vec![self.copy()],

            op: "**".to_string(),
        };

        let val = Rc::new(RefCell::new(wrapped_out));

        let sc = self.clone();
        let oc = val.clone();

        let ibw = Box::new(move || {
            let data = sc.val.borrow().data;
            sc.val.borrow_mut().grad += (rhs * data.powf(rhs - 1.0)) * oc.borrow().grad;
        });

        Value {
            val,
            bw: Rc::new(RefCell::new(ibw)),
        }
    }

    pub fn relu(&self) -> Value {
        let wrapped_out = WrappedValue {
            data: if self.data() < 0.0 { 0.0 } else { self.data() },
            grad: 0.0,
            prev: vec![self.copy()],
            op: "ReLU".to_string(),
        };

        let val = Rc::new(RefCell::new(wrapped_out));

        let sc = self.clone();
        let oc = val.clone();

        let ibw = Box::new(move || {
            sc.val.borrow_mut().grad += if oc.borrow().data > 0.0 {
                oc.borrow().grad
            } else {
                0.0
            };
        });

        Value {
            val,
            bw: Rc::new(RefCell::new(ibw)),
        }
    }

    /// create topological sorting
    fn build_topo(&self, topo: &mut Vec<Value>, visited: &mut HashSet<Value>) {
        if !visited.contains(self) {
            visited.insert(self.copy());
            for child in self.val.borrow().prev.iter() {
                child.build_topo(topo, visited);
            }
            topo.push(self.copy());
        }
    }

    // the core of the backpropagation algorithm ...
    // not too different from an audio processing graph,
    // except that it can't contain loops, as it's a DAG
    // the DAG is set up implicitly by applying the operations
    // each value represents the DAG up to that stage ...
    pub fn backward(&mut self) {
        let mut topo: Vec<Value> = Vec::new();
        let mut visited: HashSet<Value> = HashSet::new();
        self.build_topo(&mut topo, &mut visited);

        self.val.borrow_mut().grad = 1.0;

        //one variable at a time, apply chain rule
        for v in topo.iter().rev() {
            v.bw.borrow_mut()();
        }
    }
}

// ADDITION
impl ops::Add<Value> for Value {
    type Output = Value;

    fn add(self, rhs: Value) -> Value {
        let wrapped_out = WrappedValue {
            data: self.data() + rhs.data(),
            grad: 0.0,
            prev: vec![self.copy(), rhs.copy()],
            op: "+".to_string(),
        };

        let val = Rc::new(RefCell::new(wrapped_out));

        let sc = self.clone();
        let rc = rhs.clone();
        let oc = val.clone();

        let ibw = Box::new(move || {
            sc.val.borrow_mut().grad += oc.borrow().grad;
            rc.val.borrow_mut().grad += oc.borrow().grad;
        });

        Value {
            val,
            bw: Rc::new(RefCell::new(ibw)),
        }
    }
}

impl AddAssign for Value {
    fn add_assign(&mut self, other: Self) {
        *self = self.clone() + other;
    }
}

impl ops::Add<f32> for Value {
    type Output = Value;

    fn add(self, rhs: f32) -> Value {
        self + Value::new(rhs)
    }
}

impl ops::Add<Value> for f32 {
    type Output = Value;

    fn add(self, rhs: Value) -> Value {
        let lhs = Value::new(self);
        lhs + rhs
    }
}

// MULTIPLICATION
impl ops::Mul<Value> for Value {
    type Output = Value;

    fn mul(self, rhs: Value) -> Value {
        let wrapped_out = WrappedValue {
            data: self.data() * rhs.data(),
            grad: 0.0,
            prev: vec![self.copy(), rhs.copy()],
            op: "*".to_string(),
        };

        let val = Rc::new(RefCell::new(wrapped_out));

        let sc = self.clone();
        let rc = rhs.clone();
        let oc = val.clone();

        let ibw = Box::new(move || {
            sc.val.borrow_mut().grad += rc.val.borrow().data * oc.borrow().grad;
            rc.val.borrow_mut().grad += sc.val.borrow().data * oc.borrow().grad;
        });

        Value {
            val,
            bw: Rc::new(RefCell::new(ibw)),
        }
    }
}

impl ops::Mul<f32> for Value {
    type Output = Value;

    fn mul(self, rhs: f32) -> Value {
        self * Value::new(rhs)
    }
}

impl ops::Mul<Value> for f32 {
    type Output = Value;

    fn mul(self, rhs: Value) -> Value {
        let lhs = Value::new(self);
        lhs * rhs
    }
}

// NEGATION
impl ops::Neg for Value {
    type Output = Value;

    fn neg(self) -> Value {
        self * -1.0
    }
}

// SUBTRACTION
impl ops::Sub<Value> for Value {
    type Output = Value;

    fn sub(self, other: Value) -> Value {
        self + (-other)
    }
}

impl ops::Sub<Value> for f32 {
    type Output = Value;

    fn sub(self, other: Value) -> Value {
        self + (-other)
    }
}

impl ops::Sub<f32> for Value {
    type Output = Value;

    fn sub(self, other: f32) -> Value {
        self + -(other)
    }
}

// DIVISION
impl ops::Div<Value> for Value {
    type Output = Value;

    fn div(self, other: Value) -> Value {
        self * other.powf(-1.0)
    }
}

impl ops::Div<Value> for f32 {
    type Output = Value;

    fn div(self, other: Value) -> Value {
        self * other.powf(-1.0)
    }
}

impl ops::Div<f32> for Value {
    type Output = Value;

    fn div(self, other: f32) -> Value {
        self * other.powf(-1.0)
    }
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn contrived_test() {
        let a = Value::new(-4.0);
        let b = Value::new(2.0);

        let mut c = a.clone() + b.clone();
        let mut d = a.clone() * b.clone() + b.clone().powf(3.0);

        c += c.clone() + 1.0;
        c += 1.0 + c.clone() + -(a.clone());

        d += d.clone() * 2.0 + (b.clone() + a.clone()).relu();
        d += 3.0 * d.clone() + (b.clone() - a.clone()).relu();

        let e = c - d;
        let f = e.powf(2.0);

        let mut g = f.clone() / 2.0;
        g += 10.0 / f;

        // 24.704082
        println!("{}", g.data());

        g.backward();

        // prints 138.8338, i.e. the numerical value of dg/da
        println!("{}", a.grad());

        // prints 645.5773, i.e. the numerical value of dg/db
        println!("{}", b.grad())
    }
}
