use rand::distributions::Uniform;
use rand::{thread_rng, Rng};

use crate::engine::Value;

pub struct Neuron {
    w: Vec<Value>, // weight
    b: Value,      // bias
    nonlin: bool,  // nonlinearity
}

impl Neuron {
    pub fn new(nin: usize, nonlin: bool) -> Neuron {
        let mut rng = thread_rng();
        let side = Uniform::new(-1.0, 1.0);

        // random initial weights ...
        let mut w = Vec::new();
        for _ in 0..nin {
            w.push(Value::new(rng.sample(side)))
        }

        Neuron {
            w,
            b: Value::new(0.0),
            nonlin,
        }
    }

    pub fn forward(&mut self, x: &[Value]) -> Value {
        let mut acc = Value::new(0.0);
        acc += self.b.clone();

        for (wi, xi) in self.w.iter().zip(x.iter()) {
            acc += wi.clone() * xi.clone();
        }

        if self.nonlin {
            acc.relu()
        } else {
            acc
        }
    }

    pub fn parameters(&mut self) -> Vec<&mut Value> {
        // collect refs ...
        let mut v: Vec<&mut Value> = self.w.iter_mut().collect();
        v.push(&mut self.b);
        v
    }

    pub fn zero_grad(&mut self) {
        for p in self.parameters() {
            p.val.borrow_mut().grad = 0.0;
        }
    }
}

pub struct Layer {
    neurons: Vec<Neuron>,
}

impl Layer {
    pub fn new(nin: usize, nout: usize, nonlin: bool) -> Self {
        let mut neurons = Vec::new();
        for _ in 0..nout {
            neurons.push(Neuron::new(nin, nonlin));
        }
        Layer { neurons }
    }

    pub fn forward(&mut self, x: &[Value]) -> Vec<Value> {
        self.neurons.iter_mut().map(|n| n.forward(x)).collect()
    }

    pub fn parameters(&mut self) -> Vec<&mut Value> {
        // collect refs ...
        self.neurons
            .iter_mut()
            .flat_map(|n| n.parameters())
            .collect()
    }

    pub fn zero_grad(&mut self) {
        for n in self.neurons.iter_mut() {
            n.zero_grad();
        }
    }
}

// friendship is magic ...
pub struct Mlp {
    layers: Vec<Layer>,
}

impl Mlp {
    pub fn new(nin: usize, nouts: Vec<usize>) -> Self {
        let mut sz = vec![nin];
        sz.append(&mut nouts.clone());

        let mut layers = Vec::new();

        for i in 0..nouts.len() {
            layers.push(Layer::new(sz[i], sz[i + 1], i != nouts.len() - 1))
        }

        Mlp { layers }
    }

    pub fn forward(&mut self, x: &[f32]) -> Vec<Value> {
        let mut xv: Vec<Value> = x.iter().map(|v| Value::new(*v)).collect();
        for l in self.layers.iter_mut() {
            xv = l.forward(&xv);
        }
        xv
    }

    pub fn parameters(&mut self) -> Vec<&mut Value> {
        // collect refs ...
        self.layers
            .iter_mut()
            .flat_map(|n| n.parameters())
            .collect()
    }

    pub fn zero_grad(&mut self) {
        for l in self.layers.iter_mut() {
            l.zero_grad();
        }
    }
}
