use crate::{engine::Value, nn::Mlp};

mod engine;
mod nn;

fn main() {
    println!("Hello, world!");

    // very small data
    let xs: Vec<Vec<f32>> = vec![
        vec![2.0, 3.0, -1.0],
        vec![3.0, -1.0, 0.5],
        vec![0.5, 1.0, 1.0],
        vec![1.0, 1.0, -1.0],
    ];
    let ys = vec![1.0, -1.0, -1.0, 1.0];

    let mut n = Mlp::new(3, vec![4, 4, 1]);

    let lr = 0.02;

    for i in 0..300 {
        let ypred: Vec<Value> = xs.iter().map(|x| n.forward(x)[0].clone()).collect();

        let mut loss = Value::new(0.0);
        for (y, yp) in ys.iter().zip(ypred.iter()) {
            loss += (y.clone() - yp.clone()).powf(2.0)
        }

        // don't forget ...
        n.zero_grad();

        loss.backward();

        for p in n.parameters() {
            let grad = p.grad();
            p.val.borrow_mut().data += -lr * grad;
        }

        println!("run: {i} loss: {}", loss.data());
    }

    let ypred: Vec<f32> = xs.iter().map(|x| n.forward(x)[0].data()).collect();

    println!("{ypred:?}");
}
